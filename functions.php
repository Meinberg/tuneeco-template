<?php
/**
 * tuneeco-template functions and definitions
 *
 * @link       https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package    tuneeco-template
 * @copyright  Copyright (c) 2020, Claudio Meinberg
 * @license    http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

define( 'tuneeco_VERSION', '1.0.0' );

if ( ! function_exists( 'tuneeco_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function tuneeco_setup() {

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'responsive-embeds' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary Menu', 'tuneeco-template' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'tuneeco_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add image size for blog posts, 600px wide (and unlimited height).
		add_image_size( 'tuneeco-template-blog', 600 );
		// Add image size for full width template, 1040px wide (and unlimited height).
		add_image_size( 'tuneeco-template-full-width', 1040 );

		// Add stylesheet for the WordPress editor.
		add_editor_style( '/assets/css/editor-style.css' );

		// Add support for custom logo.
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 100,
				'width'       => 400,
				'flex-height' => true,
				'flex-width'  => true,
				'header-text' => array( 'site-title', 'site-description' ),
			)
		);

		// Add support for WooCommerce.
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );

	}
endif;
add_action( 'after_setup_theme', 'tuneeco_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function tuneeco_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'tuneeco_content_width', 1040 );
}
add_action( 'after_setup_theme', 'tuneeco_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function tuneeco_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'tuneeco-template' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'tuneeco-template' ),
			'before_widget' => '<section class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Bottom Area', 'tuneeco-template' ),
			'id'            => 'bottom-1',
			'description'   => esc_html__( 'Add widgets here.', 'tuneeco-template' ),
			'before_widget' => '<section class="bottom-widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="bottom-widget-title">',
			'after_title'   => '</h3>',
		)
	);
}
add_action( 'widgets_init', 'tuneeco_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function tuneeco_scripts() {
	wp_enqueue_style( 'tuneeco-template-style', get_stylesheet_uri(), array(), tuneeco_VERSION );

	wp_enqueue_script( 'tuneeco-template-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), tuneeco_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( class_exists( 'WooCommerce' ) ) {
		wp_enqueue_style( 'tuneeco-template-woocommerce', get_template_directory_uri() . '/assets/css/woocommerce.css', 'tuneeco-template-style', tuneeco_VERSION );
	}
}
add_action( 'wp_enqueue_scripts', 'tuneeco_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Ads scripts template tags for tuneeco theme
 */
require get_template_directory() . '/inc/ads.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer/customizer.php';

/**
 * Load Customizer Settings.
 */
require get_template_directory() . '/inc/customizer/customizer-helper-settings.php';

/**
 * If the WooCommerce plugin is active, load the related functions.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/woocommerce/functions.php';
}

/**
 * Display the admin notice.
 */
function tuneeco_admin_notice() {
	global $current_user;
	$user_id = $current_user->ID;

	if ( ! get_user_meta( $user_id, 'tuneeco_ignore_customizer_notice' ) ) {
		?>

		<div class="notice notice-info">
			<p>
				<strong>Bem vindo!</strong> Template desenvolvido para sites de Experts Gold360
				<span style="float:right">
					<a href="?tuneeco_ignore_customizer_notice=0"><?php esc_html_e( 'Ocultar', 'tuneeco-template' ); ?></a>
				</span>
			</p>
		</div>

		<?php
	}
}
add_action( 'admin_notices', 'tuneeco_admin_notice' );

/**
 * Dismiss the admin notice.
 */
function tuneeco_dismiss_admin_notice() {
	global $current_user;
	$user_id = $current_user->ID;
	/* If user clicks to ignore the notice, add that to their user meta */
	if ( isset( $_GET['tuneeco_ignore_customizer_notice'] ) && '0' === $_GET['tuneeco_ignore_customizer_notice'] ) {
		add_user_meta( $user_id, 'tuneeco_ignore_customizer_notice', 'true', true );
	}
}
add_action( 'admin_init', 'tuneeco_dismiss_admin_notice' );

/**
 * Load Admin Panel.
 */
require get_template_directory() . '/admin.php';

function wpassist_remove_block_library_css(){
    wp_dequeue_style( 'wp-block-library' );
}
add_action( 'wp_enqueue_scripts', 'wpassist_remove_block_library_css' );