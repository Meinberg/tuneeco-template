<?php
/**
 * Template Name: Full-width Template
 * Template Post Type: post, page
 *
 * @package    tuneeco-template
 * @copyright  Copyright (c) 2020, Claudio Meinberg
 * @license    http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

get_header(); ?>
<?php
while ( have_posts() ) :

	if ( function_exists( 'tuneeco_pageview' ) )
		tuneeco_pageview( get_the_ID() );

	the_post();

	?>

	<div class="content-area">

			<article <?php post_class(); ?>>



				<header class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

					<div class="entry-subtitle">
						<?php the_excerpt(); ?>
					</div>

					<?php // Autor, publicação e atualização - BOF ?>
					<div class="clearfix"></div>
					<div class="author-publish-date">
						<?php  if (tuneeco_get_theme_option('exibe_author')) : ?>
						<div class="author">Por <?php the_author(); ?></div>
						<?php endif; ?>
						<div class="publish-date">
							<?php the_date('d/m/Y H:i'); ?> - Atualizado há
							<?php print(human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) )); ?>
						</div>

						<?php  if (tuneeco_get_theme_option('exibe_addthis')) : ?>
						<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ed32a2282052007"></script>
						<div class="addthis_inline_share_toolbox"></div>
						<?php endif; ?>
					</div>
					<?php // Autor, publicação e atualização - EOF ?>

					<hr>
				</header><!-- .entry-header -->

				<?php tuneeco_thumbnail( 'tuneeco-template-full-width' ); ?>

				<?php
				if ( get_edit_post_link() ) :

					edit_post_link( esc_html__( '(Edit)', 'tuneeco-template' ), '<p class="edit-link">', '</p>' );

				endif;
				?>

				<div class="entry-content">
					<?php
					the_content();

					wp_link_pages(
						array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'tuneeco-template' ),
							'after'  => '</div>',
						)
					);
					?>
				</div><!-- .entry-content -->

			</article><!-- #post-## -->

			<?php

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		?>

		<div class="bottom">
		<?php
		get_sidebar(); ?>
	</div>
	</div><!-- .content-area -->

<?php
endwhile;
get_footer();
