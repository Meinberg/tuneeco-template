<?php
/**
 * Ads scripts template tags for AMB theme
 *
 * @package    tuneeco-template
 * @copyright  Copyright (c) 2020, Claudio Meinberg
 * @license    http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */


if ( ! function_exists( 'tuneeco_ads_header' ) ) :
    /**
     * Set ads header script.
     */
    function tuneeco_ads_header() {
        tuneeco_gpt_header();
        tuneeco_avantis();
        tuneeco_teads();
        tuneeco_taboola_header();
        // tuneeco_seedtag_header();
    }
endif;

if ( ! function_exists( 'tuneeco_get_gpt_ad_sizes' ) ) :
    /**
     * Retorna tamanhos de adunits baseado no user-agent do request
     */
    function tuneeco_get_gpt_ad_sizes() { ?>

<?php
    }
endif;

if ( ! function_exists( 'tuneeco_gpt_header' ) ) :
    /**
     * Output gpt script.
     */
    function tuneeco_gpt_header() { ?>

        <!-- <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script> -->
<?php
    }
endif;



if ( ! function_exists( 'tuneeco_avantis' ) ) :
    /**
     * Output avantis script.
     */
    function tuneeco_avantis() {

        if (!tuneeco_get_theme_option('exibe_avantis')) {
            return;
        }

        echo '<script async src="//cdn.avantisvideo.com/avm/js/video-loader.js?id=56373fc3-e27e-4eff-a4bc-9100f0bb5470&tagId=1&subId=&callback=" id="avantisJS"> </script>';

    }
endif;

if ( ! function_exists( 'tuneeco_teads' ) ) :
    /**
     * Output teads script.
     */
    function tuneeco_teads() {

        if (!tuneeco_get_theme_option('exibe_teads')) {
            return;
        }

        echo '<script type="text/javascript" class="teads" async="true" src="//a.teads.tv/page/70541/tag"></script>';

    }
endif;


if ( ! function_exists( 'tuneeco_taboola_header' ) ) :
    /**
     * Output taboola header script.
     */
    function tuneeco_taboola_header() { ?>

        <?php
          if (!tuneeco_get_theme_option('exibe_taboola')) {
            return;
          }
        ?>

        <script type="text/javascript">
            window._taboola = window._taboola || [];
            _taboola.push({article:'auto', tracking:'utm_source=taboola&utm_medium=organicclicks'})
            !function (e, f, u, i) {
                if (!document.getElementById(i)){
                    e.async = 1;
                    e.src = u;
                    e.id = i;
                    f.parentNode.insertBefore(e, f);
                }
            }(document.createElement('script'),
            document.getElementsByTagName('script')[0],
            '//cdn.taboola.com/libtrc/gold360-anamariabraga/loader.js',
            'tb_loader_script');
        if(window.performance && typeof window.performance.mark == 'function') {
            window.performance.mark('tbl_ic');
        }
        </script>

        <script type="text/javascript">
              window._taboola = window._taboola || [];
        !function (e, f, u, i) {
            if (!document.getElementById(i)){
                 e.async = 1;
                 e.src = u;
                 e.id = i;
                 f.parentNode.insertBefore(e, f);
            }
        }(document.createElement('script'),document.getElementsByTagName('script')[0],'//cdn.taboola.com/libtrc/gold360-anamariabraga/loader.js','tb_loader_script');

        if(window.performance && typeof window.performance.mark == 'function') {
            window.performance.mark('tbl_ic');
        }
        </script>
<?php
    }
endif;


if ( ! function_exists( 'tuneeco_taboola_body' ) ) :
    /**
     * Output taboola body script.
     */
    function tuneeco_taboola_body() { ?>

        <?php
          if (!tuneeco_get_theme_option('exibe_taboola')) {
            return;
          }
        ?>

        <div id="taboola-below-article-thumbnails"></div>
        <script type="text/javascript">
            window._taboola = window._taboola || [];
            _taboola.push({
                mode: 'alternating-thumbnails-c',
                container: 'taboola-below-article-thumbnails',
                placement: 'Below Article Thumbnails',
                target_type: 'mix'
            });
        </script>


        <script type="text/javascript">
            window._taboola = window._taboola || [];
            _taboola.push({flush: true});
        </script>
<?php
    }
endif;



if ( ! function_exists( 'tuneeco_seedtag_header' ) ) :
    /**
     * Output seedtag header script.
     */
    function tuneeco_seedtag_header() { ?>

        <?php
          if (!tuneeco_get_theme_option('exibe_seedtag')) {
            return;
          }
        ?>

        <script type="text/javascript">
            window._seedtagq = window._seedtagq || [];
            window._seedtagq.push(['_setId', '4315-5968-01']);

            window._seedtagq.push(['iframe_mode']);
            (function () {
                var st = document.createElement('script');
                st.type = 'text/javascript';
                st.async = true;
                st.src = ('https:' == document.location.protocol
                ? 'https'
                : 'http') + '://config.seedtag.com/loader.js?v=' + Math.random();
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(st, s);
            })();
        </script>
<?php
    }
endif;


if ( ! function_exists( 'tuneeco_ads_in_the_content' ) ) :
    /**
     * Inclui automaticamente anuncios no conteúdo do post.
     *
     */
    function tuneeco_ads_in_the_content(  ) {
        $content = get_the_content();
        $content = apply_filters( 'the_content', $content );
        $content = str_replace( ']]>', ']]&gt;', $content );


        if (!tuneeco_get_theme_option('exibe_autoads')) {
            echo $content;

            return;
        }

        $paragraphAfter[6] = '<hr class="hr-text" data-content="Continua depois da publicidade"><div class="text-center" id="div-gpt-ad-content-auto-1"><script>googletag.cmd.push(function() { googletag.display("div-gpt-ad-content-auto-1"); });</script></div><hr>';

        $paragraphAfter[12] = '<hr class="hr-text" data-content="Continua depois da publicidade"><div class="text-center" id="div-gpt-ad-content-auto-2"><script>googletag.cmd.push(function() { googletag.display("div-gpt-ad-content-auto-2"); });</script></div><hr>';

        $paragraphAfter[18] = '<hr class="hr-text" data-content="Continua depois da publicidade"><div class="text-center" id="div-gpt-ad-content-auto-3"><script>googletag.cmd.push(function() { googletag.display("div-gpt-ad-content-auto-3"); });</script></div><hr>';
        // $paragraphAfter[5] = '<div>AFTER FIFtH</div>'; //display after the fifth paragraph

        // $content = apply_filters( 'the_content', get_the_content() );

        $content = explode("</p>", $content);
        $count = count($content);
        $new_content = '';
        for ($i = 0; $i < $count; $i++ ) {
            $new_content .= $content[$i] . "</p>";

            if ( array_key_exists($i, $paragraphAfter) ) {
                // echo $paragraphAfter[$i];
                $new_content .= $paragraphAfter[$i];
            }
            // echo $content[$i] . "</p>";

        }

        echo $new_content;

    }
endif;