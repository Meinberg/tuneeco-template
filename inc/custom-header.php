<?php
/**
 * Sample implementation of the Custom Header feature
 *
 * You can add an optional custom header image to header.php like so ...
 * <?php the_header_image_tag(); ?>
 *
 * @link       https://developer.wordpress.org/themes/functionality/custom-headers/
 *
 * @package    tuneeco-template
 * @copyright  Copyright (c) 2020, Claudio Meinberg
 * @license    http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses tuneeco_header_style()
 */
function tuneeco_custom_header_setup() {
	add_theme_support(
		'custom-header',
		apply_filters(
			'tuneeco_custom_header_args',
			array(
				'default-image'      => '',
				'default-text-color' => '000000',
				'width'              => 1000,
				'height'             => 250,
				'flex-height'        => true,
				'flex-width'         => true,
				'wp-head-callback'   => 'tuneeco_header_style',
			)
		)
	);
}
add_action( 'after_setup_theme', 'tuneeco_custom_header_setup' );

if ( ! function_exists( 'tuneeco_header_style' ) ) :
	/**
	 * Styles the header image and text displayed on the blog.
	 *
	 * @see tuneeco_custom_header_setup().
	 */
	function tuneeco_header_style() {

		$header_text_color = get_header_textcolor();
		$height            = get_theme_mod( 'header-background-height', '173' );
		$repeat            = get_theme_mod( 'header-background-repeat', 'no-repeat' );
		$size              = get_theme_mod( 'header-background-size', 'initial' );
		$position          = get_theme_mod( 'header-background-position', 'center' );
		$attachment        = get_theme_mod( 'header-background-attachment', false ) ? 'fixed' : 'scroll';

		$header_bar_bgcolor  = get_theme_mod( 'header-bar-bgcolor', false );
		$header_bar_link_color  = get_theme_mod( 'header-bar-link-color', false );
		$category_bar_bgcolor  = get_theme_mod( 'category-bar-bgcolor', false );
		$category_bar_text_bgcolor  = get_theme_mod( 'category-bar-text-bgcolor', false );

		$link_color          = get_theme_mod( 'link-color', false );
		$menu_link_color   = get_theme_mod( 'menu-link-color', false );

		?>
		<style type="text/css">
			<?php if ( ! display_header_text() ) : ?>
				.site-title,
				.site-description {
					position: absolute;
					clip: rect(1px, 1px, 1px, 1px);
				}
			<?php else : ?>
				.nav .site-title a {
					color: #<?php echo esc_attr( $header_text_color ); ?>;
				}
			<?php endif; ?>

			.site-header {
				min-height: <?php echo absint( $height ) . 'px'; ?>;
			}

			<?php if ( has_header_image() ) : ?>
				.site-header {
					background-image: url( <?php header_image(); ?> );
					background-repeat: <?php echo esc_attr( $repeat ); ?>;
					background-size: <?php echo esc_attr( $size ); ?>;
					background-position: <?php echo esc_attr( $position ); ?>;
					background-attachment: <?php echo esc_attr( $attachment ); ?>;
				}
			<?php endif; ?>

			<?php if ( $header_bar_bgcolor ) : ?>
				.nav {
					background-color: <?php echo esc_attr( $header_bar_bgcolor ); ?>;
				}
			<?php endif; ?>

			<?php if ( $header_bar_link_color ) : ?>
				.nav a {
					color: <?php echo esc_attr( $header_bar_link_color ); ?>;
				}
			<?php endif; ?>


			<?php if ( $menu_link_color ) : ?>
				.menu a {
					color: <?php echo esc_attr( $menu_link_color ); ?>;
				}
			<?php endif; ?>

			<?php if ( $category_bar_bgcolor ) : ?>
				.archive-header h1{
					background-color: <?php echo esc_attr( $category_bar_bgcolor ); ?>;
					color: <?php echo esc_attr( $category_bar_text_bgcolor ); ?>;
				}
			<?php endif; ?>
			<?php if ( $link_color ) : ?>
				a {
					color: <?php echo esc_attr( $link_color ); ?>;
				}
			<?php endif; ?>
		</style>
		<?php
	}
endif;

if ( ! function_exists( 'tuneeco_barra_globo' ) ) :
	/**
	 * Inclui barra da globo.
	 *
	 */
	function tuneeco_barra_globo() { ?>

    <link href="//s.glbimg.com/gl/ba/css/barra-globocom.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var glb = this.glb || {};
          glb.barra = {
              exibeAssineJa: false,
              useVintage: true,
              exibeCentral: false
          };
          (function () {
              var s = document.createElement("script");
              s.type = "text/javascript"; s.async = true; s.charset = "utf-8";
              s.src = "//s.glbimg.com/gl/ba/js/barra-globocom.min.js";
              var ss = document.getElementsByTagName("script")[0]; ss.parentNode.insertBefore(s, ss);
          })();
    </script>

<?php
	}
endif;