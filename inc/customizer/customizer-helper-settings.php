<?php
/**
 * Example implementation of the 'Customizer Helper'.
 *
 * @link       https://developer.wordpress.org/themes/functionality/custom-headers/
 *
 * @package    customizer-helper
 * @copyright  Copyright (c) 2020, Claudio Meinberg
 * @license    http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @version    1.0.0
 */

/**
 * Load the helper class.
 */
if ( file_exists( dirname( __FILE__ ) . '/class-customizer-helper.php' ) ) {
	require_once dirname( __FILE__ ) . '/class-customizer-helper.php';
}

/**
 * Defines customizer settings
 */
function customizer_helper_settings() {

	// Stores all the panels to be added.
	$panels = array();

	// Stores all the sections to be added.
	$sections = array();

	// Stores all the settings that will be added.
	$settings = array();

	$section = 'header_image';

	$settings['header-background-height'] = array(
		'id'       => 'header-background-height',
		'label'    => esc_html__( 'Header Height (px)', 'tuneeco-template' ),
		'section'  => $section,
		'type'     => 'number',
		'priority' => 1,
		'default'  => 173,
	);

	$settings['header-background-repeat'] = array(
		'id'       => 'header-background-repeat',
		'label'    => esc_html__( 'Header Background Repeat', 'tuneeco-template' ),
		'section'  => $section,
		'type'     => 'radio',
		'priority' => 12,
		'choices'  => array(
			'no-repeat' => esc_html__( 'No Repeat', 'tuneeco-template' ),
			'repeat'    => esc_html__( 'Tile', 'tuneeco-template' ),
			'repeat-x'  => esc_html__( 'Tile Horizontally', 'tuneeco-template' ),
			'repeat-y'  => esc_html__( 'Tile Vertically', 'tuneeco-template' ),
		),
		'default'  => 'no-repeat',
	);

	$settings['header-background-size'] = array(
		'id'       => 'header-background-size',
		'label'    => esc_html__( 'Header Background Size', 'tuneeco-template' ),
		'section'  => $section,
		'type'     => 'radio',
		'priority' => 12,
		'choices'  => array(
			'initial' => esc_html__( 'Normal', 'tuneeco-template' ),
			'cover'   => esc_html__( 'Cover', 'tuneeco-template' ),
			'contain' => esc_html__( 'Contain', 'tuneeco-template' ),
		),
		'default'  => 'initial',
	);

	$settings['header-background-position'] = array(
		'id'       => 'header-background-position',
		'label'    => esc_html__( 'Header Background Position', 'tuneeco-template' ),
		'section'  => $section,
		'type'     => 'select',
		'priority' => 13,
		'choices'  => array(
			'left'   => esc_html__( 'Left', 'tuneeco-template' ),
			'center' => esc_html__( 'Center', 'tuneeco-template' ),
			'right'  => esc_html__( 'Right', 'tuneeco-template' ),
		),
		'default'  => 'center',
	);

	$settings['header-background-attachment'] = array(
		'id'       => 'header-background-attachment',
		'label'    => esc_html__( 'Scroll with Page', 'tuneeco-template' ),
		'section'  => $section,
		'type'     => 'checkbox',
		'priority' => 14,
	);

	$settings['header-bar-bgcolor'] = array(
		'section'  => 'colors',
		'id'       => 'header-bar-bgcolor',
		'label'    => esc_html__( 'Header bar', 'tuneeco-template' ),
		'type'     => 'color',
		'priority' => 41,
		'default'  => '#c4170c',
	);

	$settings['header-bar-link-color'] = array(
		'section'  => 'colors',
		'id'       => 'header-bar-link-color',
		'label'    => esc_html__( 'Header bar Link Color', 'tuneeco-template' ),
		'type'     => 'color',
		'priority' => 41,
		'default'  => '#ffffff',
	);

	$settings['category-bar-bgcolor'] = array(
		'section'  => 'colors',
		'id'       => 'category-bar-bgcolor',
		'label'    => esc_html__( 'Category bar', 'tuneeco-template' ),
		'type'     => 'color',
		'priority' => 41,
		'default'  => '#c4170c',
	);

	$settings['category-bar-text-bgcolor'] = array(
		'section'  => 'colors',
		'id'       => 'category-bar-text-bgcolor',
		'label'    => esc_html__( 'Category bar Text', 'tuneeco-template' ),
		'type'     => 'color',
		'priority' => 41,
		'default'  => '#ffffff',
	);

	$settings['link-color'] = array(
		'section'  => 'colors',
		'id'       => 'link-color',
		'label'    => esc_html__( 'Link Color', 'tuneeco-template' ),
		'type'     => 'color',
		'priority' => 41,
		'default'  => '#c4170c',
	);

	$settings['menu-link-color'] = array(
		'section'  => 'colors',
		'id'       => 'menu-link-color',
		'label'    => esc_html__( 'Menu Link Color', 'tuneeco-template' ),
		'type'     => 'color',
		'priority' => 41,
		'default'  => '#c4170c',
	);

	$settings['navigation-color'] = array(
		'section'  => 'colors',
		'id'       => 'navigation-color',
		'label'    => esc_html__( 'AMP Color', 'tuneeco-template' ),
		'type'     => 'color',
		'priority' => 41,
		'default'  => '#e94662',
	);



	// Adds the panels to the $settings array.
	$settings['panels'] = $panels;

	// Adds the sections to the $settings array.
	$settings['sections'] = $sections;

	$customizer_helper = Customizer_Helper::Instance();
	$customizer_helper->add_settings( $settings );

}
add_action( 'init', 'customizer_helper_settings' );
