<?php
/**
 * Template part for displaying posts
 *
 * @link       https://codex.wordpress.org/Template_Hierarchy
 *
 * @package    tuneeco-template
 * @copyright  Copyright (c) 2020, Claudio Meinberg
 * @license    http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

?>

<article <?php post_class(); ?>>
	<?php
		// use AMPforWP\AMPVendor\AMP_Post_Template;

	?>
	<header class="entry-header">
		<?php
		if ( function_exists('yoast_breadcrumb') ) {
		  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
		} else {
			print('no breadcrumbs');
		}
		?>
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
			echo '<div class="entry-excerpt"><p>' . get_the_excerpt() . '</p></div>';
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) :
			?>
			<?php //tuneeco_thumbnail( 'tuneeco-template-blog' ); ?>
			<?php // Autor, publicação e atualização - BOF ?>
			<div class="clearfix"></div>
			<div class="author-publish-date">
				<?php  if (tuneeco_get_theme_option('exibe_author')) : ?>
				<div class="author">Por <?php the_author(); ?></div>
				<?php endif; ?>
				<div class="publish-date">
					<?php the_date('d/m/Y H:i'); ?> - Atualizado há
					<?php print(human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) )); ?>
				</div>
			</div>
			<?php // Autor, publicação e atualização - EOF ?>

			<?php  if (tuneeco_get_theme_option('exibe_addthis')) : ?>
			<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ed32a2282052007"></script>
			<div class="addthis_inline_share_toolbox"></div>
			<?php endif; ?>

			<?php if ( has_post_thumbnail() ) { ?>
			<div class="sf-img">
				<?php //the_post_thumbnail();?>
				<?php
					echo get_the_post_thumbnail($post->ID, 'Large', array('title' => $title, 'sizes' => '(max-width: 320px) 288px, (max-width: 375px) 343px, (max-width: 425px) 393px, (max-width: 540px) 493px, (max-width: 768px) 320px, (max-width: 999px) 435px, 288px'));
				?>
			</div>
			<?php }; ?>

		<?php endif; ?>



	</header><!-- .entry-header -->

	<!-- <hr class="hr-text" data-content="Continua depois da publicidade"> -->
	<!-- /122331895/amb-cm/news-top -->
	<!-- <div id='div-gpt-ad-1591687595423-0' class="text-center"> -->
	  <!-- <script> -->
	    <!-- //googletag.cmd.push(function() { googletag.display('div-gpt-ad-1591687595423-0'); }); -->
	  <!-- </script> -->
	<!-- </div> -->
	<!-- <hr class="hr-text" data-content=""> -->

	<div class="entry-content">
		<?php
		// the_content( esc_html__( 'Continue reading &rarr;', 'tuneeco-template' ) );
		tuneeco_ads_in_the_content();
		// the_content();

		wp_link_pages(
			array(
				// 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'tuneeco-template' ),
				'before' => '<div class="page-links">',
				'after'  => '</div>',
				'next_or_number'   => 'next',
				'nextpagelink'     => 'Continuar a leitura',
				'previouspagelink' => '',
			)
		);
		?>

	</div><!-- .entry-content -->

	<!--
	<footer class="entry-footer">
		<?php //tuneeco_entry_footer(); ?>
	</footer>-->
	<!-- .entry-footer -->

</article><!-- #post-## -->
