<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link       https://codex.wordpress.org/Template_Hierarchy
 *
 * @package    tuneeco-template
 * @copyright  Copyright (c) 2020, Claudio Meinberg
 * @license    http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

?>

<article <?php post_class(); ?>>

	<header class="entry-header">

		<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>

	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_excerpt(); ?>
	</div><!-- .entry-content -->

	<?php //tuneeco_thumbnail( 'tuneeco-template-blog' ); ?>
	<?php
		echo get_the_post_thumbnail($post->ID, 'Large', array('title' => $title, 'sizes' => '(max-width: 375px) 343px, (max-width: 425px) 393px, (max-width: 540px) 493px, (max-width: 768px) 1024px, (max-width: 999px) 435px, (min-width: 1135px) 726px'));
	?>

	<div class="entry-meta">
		<?php //tuneeco_posted_on(); ?>

		<?php amp_loop_date(); ?>
	</div><!-- .entry-meta -->

</article><!-- #post-## -->
