<?php
/**
 * Template part for displaying the primary navigation menu.
 *
 * @package    tuneeco-template
 * @copyright  Copyright (c) 2020, Claudio Meinberg
 * @license    http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

?>

<!-- Load an icon library to show a hamburger menu (bars) on small screens -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script>
      /* Toggle between showing and hiding the navigation menu links when the user clicks on the hamburger menu / bar icon */
      function toggleMenu() {
        var x = document.getElementById("site-menu");
        if (x.style.display === "block") {
          x.style.display = "none";
        } else {
          x.style.display = "block";
        }
      }
    </script>
<div class="nav">
	<!-- Top Navigation Menu -->
    <div class="wrapper">
    	<nav class="topnav">
    		<a href="javascript:void(0);" class="icon left" onclick="toggleMenu()" aria-label="Menu de navegação do site">
    		  <i class="fa fa-bars"></i> <span>Menu</span>
    		</a>
    		<div class="logo">
          <?php if ( has_custom_logo() ) : ?>
            <div class="site-logo"><?php the_custom_logo(); ?></div>
          <?php endif; ?>
          <?php $blog_info = get_bloginfo( 'name' ); ?>
          <?php if ( ! empty( $blog_info ) ) : ?>
            <?php if ( is_front_page() && is_home() ) : ?>
              <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
            <?php else : ?>
              <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
            <?php endif; ?>
          <?php endif; ?>
          <?php get_the_title(); ?>
    			<!-- <img alt="Ana Maria Braga" src="https://goldcursos.com.br/amb/wp-content/uploads/2020/06/logo_amb_transparente_branco_190x36.png"> -->
    		</div>
    		<a href="javascript:void(0);" class="icon h-1" onclick="toggleMenu()" aria-label="Busque artigos no site">
    		  <i class="fa fa-search"></i> <span>Buscar</span>
    		</a>
    	</nav>
    </div>
</div>

<?php
		wp_nav_menu(
			array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'site-menu',
				// 'menu_class' => '',
				'container_class' => 'menu-principal'
			)
		);
		?>