<?php
/**
 * tuneeco-template WooCommerce functions and definitions
 *
 * @package    tuneeco-template
 * @copyright  Copyright (c) 2020, Claudio Meinberg
 * @license    http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

if ( ! function_exists( 'tuneeco_wc_checkout_link' ) ) {
	/**
	 * If there are products in the cart, show a checkout link.
	 */
	function tuneeco_wc_checkout_link() {
		global $woocommerce;

		if ( count( $woocommerce->cart->cart_contents ) > 0 ) :

			echo '<a href="' . esc_url( $woocommerce->cart->get_checkout_url() ) . '" title="' . esc_attr__( 'Checkout', 'tuneeco-template' ) . '">' . esc_html__( 'Checkout', 'tuneeco-template' ) . '</a>';

		endif;
	}
}
