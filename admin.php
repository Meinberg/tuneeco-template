<?php
/**
 * Theme Options Panel
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Start Class
if ( ! class_exists( 'Tuneeco_Theme_Options' ) ) {

    class Tuneeco_Theme_Options {

        /**
         * Start things up
         *
         * @since 1.0.0
         */
        public function __construct() {

            // We only need to register the admin panel on the back-end
            if ( is_admin() ) {
                add_action( 'admin_menu', array( 'Tuneeco_Theme_Options', 'add_admin_menu' ) );
                add_action( 'admin_init', array( 'Tuneeco_Theme_Options', 'register_settings' ) );
            }

        }

        /**
         * Returns all theme options
         *
         * @since 1.0.0
         */
        public static function get_theme_options() {
            return get_option( 'theme_options' );
        }

        /**
         * Returns single theme option
         *
         * @since 1.0.0
         */
        public static function get_theme_option( $id ) {
            $options = self::get_theme_options();
            if ( isset( $options[$id] ) ) {
                return $options[$id];
            }
        }

        /**
         * Add sub menu page
         *
         * @since 1.0.0
         */
        public static function add_admin_menu() {
            add_menu_page(
                esc_html__( 'Tuneeco Settings', 'text-domain' ),
                esc_html__( 'Tuneeco Settings', 'text-domain' ),
                'manage_options',
                'theme-settings',
                array( 'Tuneeco_Theme_Options', 'create_admin_page' )
            );
        }

        /**
         * Register a setting and its sanitization callback.
         *
         * We are only registering 1 setting so we can store all options in a single option as
         * an array. You could, however, register a new setting for each option
         *
         * @since 1.0.0
         */
        public static function register_settings() {
            register_setting( 'theme_options', 'theme_options', array( 'Tuneeco_Theme_Options', 'sanitize' ) );
        }

        /**
         * Sanitization callback
         *
         * @since 1.0.0
         */
        public static function sanitize( $options ) {

            // If we have options lets sanitize them
            if ( $options ) {

                // Checkbox
                if ( ! empty( $options['checkbox_example'] ) ) {
                    $options['checkbox_example'] = 'on';
                } else {
                    unset( $options['checkbox_example'] ); // Remove from options if not checked
                }

                // Input
                if ( ! empty( $options['input_example'] ) ) {
                    $options['input_example'] = sanitize_text_field( $options['input_example'] );
                } else {
                    unset( $options['input_example'] ); // Remove from options if empty
                }

                // Select
                if ( ! empty( $options['select_example'] ) ) {
                    $options['select_example'] = sanitize_text_field( $options['select_example'] );
                }

            }

            // Return sanitized options
            return $options;

        }

        /**
         * Settings page output
         *
         * @since 1.0.0
         */
        public static function create_admin_page() { ?>

            <div class="wrap">

                <h1><?php esc_html_e( 'Theme Options', 'text-domain' ); ?></h1>

                <form method="post" action="options.php">

                    <?php settings_fields( 'theme_options' ); ?>

                    <table class="form-table tuneeco-custom-admin-login-table">

                        <?php // Barra globo.com ?>
                        <tr valign="top">
                            <th scope="row"><?php esc_html_e( 'Barra globo.com', 'text-domain' ); ?></th>
                            <td>
                                <?php $value = self::get_theme_option( 'exibe_barra_globo' ); ?>
                                <input type="checkbox" name="theme_options[exibe_barra_globo]" <?php checked( $value, 'on' ); ?>> <?php esc_html_e( 'Exibir.', 'text-domain' ); ?>
                            </td>
                        </tr>

                        <?php // Avantis ?>
                        <tr valign="top">
                            <th scope="row"><?php esc_html_e( 'Avantis', 'text-domain' ); ?></th>
                            <td>
                                <?php $value = self::get_theme_option( 'exibe_avantis' ); ?>
                                <input type="checkbox" name="theme_options[exibe_avantis]" <?php checked( $value, 'on' ); ?>> <?php esc_html_e( 'Exibir.', 'text-domain' ); ?>
                            </td>
                        </tr>

                        <?php // Teads ?>
                        <tr valign="top">
                            <th scope="row"><?php esc_html_e( 'Teads', 'text-domain' ); ?></th>
                            <td>
                                <?php $value = self::get_theme_option( 'exibe_teads' ); ?>
                                <input type="checkbox" name="theme_options[exibe_teads]" <?php checked( $value, 'on' ); ?>> <?php esc_html_e( 'Exibir.', 'text-domain' ); ?>
                            </td>
                        </tr>

                        <?php // Taboola ?>
                        <tr valign="top">
                            <th scope="row"><?php esc_html_e( 'Taboola', 'text-domain' ); ?></th>
                            <td>
                                <?php $value = self::get_theme_option( 'exibe_taboola' ); ?>
                                <input type="checkbox" name="theme_options[exibe_taboola]" <?php checked( $value, 'on' ); ?>> <?php esc_html_e( 'Exibir.', 'text-domain' ); ?>
                            </td>
                        </tr>

                        <?php // Seedtag ?>
                        <tr valign="top">
                            <th scope="row"><?php esc_html_e( 'Seedtag', 'text-domain' ); ?></th>
                            <td>
                                <?php $value = self::get_theme_option( 'exibe_seedtag' ); ?>
                                <input type="checkbox" name="theme_options[exibe_seedtag]" <?php checked( $value, 'on' ); ?>> <?php esc_html_e( 'Exibir.', 'text-domain' ); ?>
                            </td>
                        </tr>

                        <?php // Autoads ?>
                        <tr valign="top">
                            <th scope="row"><?php esc_html_e( 'Auto Ads', 'text-domain' ); ?></th>
                            <td>
                                <?php $value = self::get_theme_option( 'exibe_autoads' ); ?>
                                <input type="checkbox" name="theme_options[exibe_autoads]" <?php checked( $value, 'on' ); ?>> <?php esc_html_e( 'Exibir.', 'text-domain' ); ?>
                            </td>
                        </tr>


                        <tr>
                            <td colspan="2"><hr></td>
                        </tr>

                        <?php // Autoads ?>
                        <tr valign="top">
                            <th scope="row"><?php esc_html_e( 'Autor', 'text-domain' ); ?></th>
                            <td>
                                <?php $value = self::get_theme_option( 'exibe_author' ); ?>
                                <input type="checkbox" name="theme_options[exibe_author]" <?php checked( $value, 'on' ); ?>> <?php esc_html_e( 'Exibir.', 'text-domain' ); ?>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2"><hr></td>
                        </tr>

                        <?php // GA ID ?>
                        <tr valign="top">
                            <th scope="row"><?php esc_html_e( 'Google Analytics TrackingID', 'text-domain' ); ?></th>
                            <td>
                                <?php $value = self::get_theme_option( 'google_analytics' ); ?>
                                <input type="text" name="theme_options[google_analytics]" value="<?php echo esc_attr( $value ); ?>">
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2"><hr></td>
                        </tr>

                        <?php // Checkbox example ?>
                        <tr valign="top">
                            <th scope="row"><?php esc_html_e( 'Checkbox Example', 'text-domain' ); ?></th>
                            <td>
                                <?php $value = self::get_theme_option( 'checkbox_example' ); ?>
                                <input type="checkbox" name="theme_options[checkbox_example]" <?php checked( $value, 'on' ); ?>> <?php esc_html_e( 'Checkbox example description.', 'text-domain' ); ?>
                            </td>
                        </tr>

                        <?php // Text input example ?>
                        <tr valign="top">
                            <th scope="row"><?php esc_html_e( 'Input Example', 'text-domain' ); ?></th>
                            <td>
                                <?php $value = self::get_theme_option( 'input_example' ); ?>
                                <input type="text" name="theme_options[input_example]" value="<?php echo esc_attr( $value ); ?>">
                            </td>
                        </tr>

                        <?php // Select example ?>
                        <tr valign="top" class="tuneeco-custom-admin-screen-background-section">
                            <th scope="row"><?php esc_html_e( 'Select Example', 'text-domain' ); ?></th>
                            <td>
                                <?php $value = self::get_theme_option( 'select_example' ); ?>
                                <select name="theme_options[select_example]">
                                    <?php
                                    $options = array(
                                        '1' => esc_html__( 'Option 1', 'text-domain' ),
                                        '2' => esc_html__( 'Option 2', 'text-domain' ),
                                        '3' => esc_html__( 'Option 3', 'text-domain' ),
                                    );
                                    foreach ( $options as $id => $label ) { ?>
                                        <option value="<?php echo esc_attr( $id ); ?>" <?php selected( $value, $id, true ); ?>>
                                            <?php echo strip_tags( $label ); ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>

                    </table>

                    <?php submit_button(); ?>

                </form>

            </div><!-- .wrap -->
        <?php }

    }
}
new Tuneeco_Theme_Options();

// Helper function to use in your theme to return a theme option value
function tuneeco_get_theme_option( $id = '' ) {
    return Tuneeco_Theme_Options::get_theme_option( $id );
}