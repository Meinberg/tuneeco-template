<?php
/**
 * The sidebar containing the main widget area
 *
 * @link       https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package    tuneeco-template
 * @copyright  Copyright (c) 2020, Claudio Meinberg
 * @license    http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside class="sidebar-1 widget-area">
<!--     <div class="sidebar-content">
        <h3>Colunas</h3>
        <ul>
            <li>
                <div>
                    <h4>Solange Frazão</h4>
                    <div>bla bla bla</div>
                </div>
                <img src="https://dlnews.com.br/imagens_noticias/img_409032020114551.jpg" alt=""
                style="float: right; border-radius: 50%; max-width: 100px">
            </li>

            <li>
                <div>
                    <h4>Flávio Federico</h4>
                    <div>bla bla bla</div>
                </div>
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTDh3PE_QWW23jm-UncvvC184pJCsTlj27JLlQ5J_3JLIK-owJR&usqp=CAU" alt=""
                style="float: right; border-radius: 50%; max-width: 100px">
            </li>

            <li style="border-bottom: 0;">
                <div>
                    <h4>Márcio Atalla</h4>
                    <div>bla bla bla</div>
                </div>
                <img src="https://conteudo.imguol.com.br/c/_layout/v2/blog-header/vivabem/thumb-marcio-atalla.png" alt=""
                style="float: right; border-radius: 50%; max-width: 120px">
            </li>

        </ul>
    </div> -->
    <?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside>

<div class="ad-sidebar-container sticky">
    <div id="div-gpt-ad-sidebar" class="ad-sidebar">
        <script>
            //googletag.cmd.push(function() { googletag.display('div-gpt-ad-sidebar'); });
        </script>
    </div>
</div>


