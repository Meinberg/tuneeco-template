<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div class="site-content">
 *
 * @link       https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package    tuneeco-template
 * @copyright  Copyright (c) 2020, Claudio Meinberg
 * @license    http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>

  <?php
    if (tuneeco_get_theme_option('google_analytics')) : ?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo(tuneeco_get_theme_option('google_analytics')); ?>"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', '<?php echo(tuneeco_get_theme_option('google_analytics')); ?>');
    </script>
  <?php
    endif;
  ?>


    <?php tuneeco_ads_header(); ?>

    <!-- <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script> -->
    <script>

      // window.googletag = window.googletag || {cmd: []};
      // googletag.cmd.push(function() {

      //   var lg_sizes = [[970, 66], [970, 90], [970, 250], [980, 120]];
      //   var sd_sizes = [[728, 90]];
      //   var md_sizes = [[468, 60]];
      //   var sm_sizes = [[336, 250], [336, 280]];
      //   var min_sizes = [[300, 100], [300, 250], [320, 50], [320, 100], [320, 250], [320, 480]];

      //   var adSlotHeader = googletag.defineSlot('/122331895/amb-cm/news-top', [[970, 66], [970, 90], [728, 90], [980, 120], [970, 250]], 'div-gpt-ad-header')
      //     .addService(googletag.pubads());
      //   var mappingHeader = googletag.sizeMapping().
      //     addSize([980, 0], [[728, 90], [970, 66], [970, 90], [970, 250], [980, 120]]).
      //     addSize([728, 0], [[728, 90]]).
      //     addSize([0, 0], []).
      //     build();
      //   adSlotHeader.defineSizeMapping(mappingHeader);

      //   var adSlot = googletag.defineSlot('/122331895/amb-cm/news-top', [[320, 100], [970, 66], [970, 90], [300, 250], [728, 90], [320, 50], [336, 280], [320, 480], [300, 100], [336, 250], [980, 120], [320, 250], [468, 60], [970, 250]], 'div-gpt-ad-1591687595423-0')
      //     .addService(googletag.pubads());
      //   var mapping = googletag.sizeMapping().
      //     addSize([1040, 0], [[300, 100], [300, 250], [320, 50], [320, 100], [320, 250], [320, 480], [336, 250], [336, 280], [468, 60], [728, 90]]).
      //     addSize([980, 0], [[300, 100], [300, 250], [320, 50], [320, 100], [320, 250], [320, 480], [336, 250], [336, 280], [468, 60], [728, 90], [970, 66], [970, 90], [970, 250], [980, 120]]).
      //     addSize([728, 0], [[300, 100], [300, 250], [320, 50], [320, 100], [320, 250], [320, 480], [336, 250], [336, 280], [468, 60], [728, 90]]).
      //     addSize([468, 0], [[300, 100], [300, 250], [320, 50], [320, 100], [320, 250], [320, 480], [336, 250], [336, 280], [468, 60]]).
      //     addSize([336, 0], [[300, 100], [300, 250], [320, 50], [320, 100], [320, 250], [320, 480], [336, 250], [336, 280]]).
      //     addSize([0, 0], [[300, 100], [300, 250], [320, 50], [320, 100], [320, 250], [320, 480]]).
      //     build();
      //   adSlot.defineSizeMapping(mapping);

      //   var adSlotSidebar = googletag.defineSlot('/122331895/amb-cm/news-sidebar-1', [[320, 100], [120, 600], [300, 600], [320, 50], [320, 480], [320, 250], [336, 280], [300, 250], [336, 250], [1, 1], [360, 600], [160, 600], [300, 100]], 'div-gpt-ad-sidebar')
      //     .addService(googletag.pubads());
      //   var sidebarMapping = googletag.sizeMapping().
      //     addSize([1040, 0], [[320, 100], [120, 600], [300, 600], [320, 50], [320, 480], [320, 250], [336, 280], [300, 250], [336, 250], [1, 1], [360, 600], [160, 600], [300, 100]]).
      //     addSize([0, 0], [1, 1]).
      //     build();
      //   adSlotSidebar.defineSizeMapping(sidebarMapping);

      //   var adSlotContent1 = googletag.defineSlot('/122331895/news-content-auto', [[300, 100], [970, 250], [320, 100], [728, 90], [320, 480], [980, 120], [300, 250], [970, 66], [970, 90], [1, 1], [320, 50], [468, 60], [336, 280], [336, 250]], 'div-gpt-ad-content-auto-1')
      //     .setTargeting('test', 'lazyload')
      //     .addService(googletag.pubads());
      //   adSlotContent1.defineSizeMapping(mapping);

      //   var adSlotContent2 = googletag.defineSlot('/122331895/news-content-auto', [[300, 100], [970, 250], [320, 100], [728, 90], [320, 480], [980, 120], [300, 250], [970, 66], [970, 90], [1, 1], [320, 50], [468, 60], [336, 280], [336, 250]], 'div-gpt-ad-content-auto-2')
      //     .setTargeting('test', 'lazyload')
      //     .addService(googletag.pubads());
      //   adSlotContent2.defineSizeMapping(mapping);

      //   var adSlotContent3 = googletag.defineSlot('/122331895/news-content-auto', [[300, 100], [970, 250], [320, 100], [728, 90], [320, 480], [980, 120], [300, 250], [970, 66], [970, 90], [1, 1], [320, 50], [468, 60], [336, 280], [336, 250]], 'div-gpt-ad-content-auto-3')
      //     .setTargeting('test', 'lazyload')
      //     .addService(googletag.pubads());
      //   adSlotContent3.defineSizeMapping(mapping);

      //   googletag.pubads().enableLazyLoad({
      //             // Fetch slots within 5 viewports.
      //             fetchMarginPercent: 200,
      //             // Render slots within 2 viewports.
      //             renderMarginPercent: 100,
      //             // Double the above values on mobile, where viewports are smaller
      //             // and users tend to scroll faster.
      //             mobileScaling: 2.0
      //           });

      //   // googletag.pubads().enableSingleRequest();
      //   googletag.enableServices();
      // });
    </script>

  <?php
    if (tuneeco_get_theme_option('exibe_barra_globo')) {
      tuneeco_barra_globo();
    }
  ?>
</head>

<body <?php body_class(); ?>>

<div class="site-wrapper">



	<?php get_template_part( 'template-parts/menu-1' ); ?>

	<div class="site-content">

    <?php if (!is_home()): ?>
     <header class="archive-header">
      <h1>
      <?php
        echo single_cat_title( '', false );
      ?></h1>
    </header>
    <!-- .page-header -->
    <?php endif; ?>
		<div class="wrapper">


      <div id="div-gpt-ad-header" class="text-center">
        <script>
          // googletag.cmd.push(function() { googletag.display('div-gpt-ad-header'); });
        </script>
      </div>

