<?php
/**
 * The template for displaying all single posts
 *
 * @link       https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package    tuneeco-template
 * @copyright  Copyright (c) 2020, Claudio Meinberg
 * @license    http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

get_header(); ?>

	<div class="content-area">

		<?php
		while ( have_posts() ) :

			if ( function_exists( 'tuneeco_pageview' ) )
				tuneeco_pageview( get_the_ID() );

			the_post();

			get_template_part( 'template-parts/content', get_post_format() );


			// If comments are open or we have at least one comment, load up the comment template.
			// if ( comments_open() || get_comments_number() ) :
			// 	comments_template();
			// endif;

			// tuneeco_the_post_navigation();

		endwhile;
		?>
	</div><!-- .content-area -->

<?php
get_sidebar(); ?>
<div class="content-area">
<?php tuneeco_taboola_body(); ?>
</div>
<?php get_footer();
